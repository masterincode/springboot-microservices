package currency.conversion.service;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import currency.conversion.model.CurrencyConversionModel;
/*
 * //Note: name="currency-exchange-service" should be same as 
 * spring.application.name=currency-exchange-service
 */
//@FeignClient(name="currency-exchange-service" , url="localhost:8000")
@FeignClient(name="currency-exchange-service") // without @FeignClient spring cannot create bean of CurrencyConversionServiceProxy
@RibbonClient(name="currency-exchange-service")
public interface CurrencyConversionServiceProxy
{
	@RequestMapping("/currency-exchange/from/{from}/to/{to}")
	public CurrencyConversionModel fetchExchangeValue(@PathVariable("from") String from,@PathVariable("to") String to );

}
