package currency.conversion.controller;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import currency.conversion.model.CurrencyConversionModel;
import currency.conversion.service.CurrencyConversionServiceProxy;

@RestController
public class CurrencyConversionController
{
	@Autowired
	private CurrencyConversionServiceProxy proxy; 
	
	@RequestMapping("/currency-conversion/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionModel convertCurrency(@PathVariable String from,@PathVariable String to,@PathVariable BigDecimal quantity)
	{
		Map<String,String> uriVariables = new HashMap<>();
		uriVariables.put("from",from);
		uriVariables.put("to",to);
		
		ResponseEntity<CurrencyConversionModel> responseEntity= new RestTemplate().getForEntity("http://localhost:8000/currency-exchange/from/{from}/to/{to}", CurrencyConversionModel.class,uriVariables);
		CurrencyConversionModel responseBody=responseEntity.getBody();
		return new CurrencyConversionModel(responseBody.getId(),from,to,responseBody.getConversionMultiple(),quantity,quantity.multiply(responseBody.getConversionMultiple()),responseBody.getPort());
	}
	
	
	@RequestMapping("/currency-conversion-feign/from/{from}/to/{to}/quantity/{quantity}")
	public CurrencyConversionModel convertCurrencyFeign(@PathVariable String from,@PathVariable String to,@PathVariable BigDecimal quantity)
	{
		CurrencyConversionModel responseBody=proxy.fetchExchangeValue(from, to);
		return new CurrencyConversionModel(responseBody.getId(),from,to,responseBody.getConversionMultiple(),quantity,quantity.multiply(responseBody.getConversionMultiple()),responseBody.getPort());
	}


}
