package currencyexchange.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import currencyexchange.model.ExchangeCurrencyModel;
import currencyexchange.service.ExchangeCurrencyService;

@RestController
public class ExchangeServiceCtrl
{
	@Autowired
	private ExchangeCurrencyService exchangeCurrencyService;
	
	@RequestMapping("/currency-exchange/from/{from}/to/{to}")
	public ExchangeCurrencyModel fetchExchangeValue(@PathVariable String from,@PathVariable String to )
	{
		return exchangeCurrencyService.fetchExchangeValue(from, to);
	}

}
