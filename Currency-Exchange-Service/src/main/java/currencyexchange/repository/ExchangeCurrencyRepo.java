package currencyexchange.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import currencyexchange.model.ExchangeCurrencyModel;

@Repository
public interface ExchangeCurrencyRepo extends CrudRepository<ExchangeCurrencyModel,Long>
{
	ExchangeCurrencyModel findByFromAndTo(String from,String to);
}
