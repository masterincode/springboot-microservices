package currencyexchange.model;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="EXCHANGE_CURRENCY")
public class ExchangeCurrencyModel
{
	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="INPUT_CURRENCY")
	private String from;
	
	@Column(name="OUTPUT_CURRENCY")
	private String to;
	
	@Column(name="CURRENCY_RATE")
	private BigDecimal conversionMultiple;
	
	@Column(name="PORT_NUMBER")
	private int port;
	
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public BigDecimal getConversionMultiple() {
		return conversionMultiple;
	}
	public void setConversionMultiple(BigDecimal conversionMultiple) {
		this.conversionMultiple = conversionMultiple;
	}
	public ExchangeCurrencyModel(Long id, String from, String to, BigDecimal conversionMultiple,int port) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.conversionMultiple = conversionMultiple;
		this.port=port;
	}
	public ExchangeCurrencyModel() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public String toString() {
		return "ExchangeCurrencyModel [id=" + id + ", from=" + from + ", to=" + to + ", conversionMultiple="
				+ conversionMultiple + ", port=" + port + "]";
	}
	
	
	
}
