package currencyexchange.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import currencyexchange.model.ExchangeCurrencyModel;
import currencyexchange.repository.ExchangeCurrencyRepo;

@Service
public class ExchangeCurrencyService
{
	@Autowired
	private ExchangeCurrencyRepo exchangeCurrencyRepo;
	
	@Autowired
	private Environment env;
	
	public ExchangeCurrencyModel fetchExchangeValue(String from,String to)
	{
		int portNumber=Integer.parseInt(env.getProperty("local.server.port"));
		ExchangeCurrencyModel model=exchangeCurrencyRepo.findByFromAndTo(from, to);
		model.setPort(portNumber);
		return model;
	}

}
