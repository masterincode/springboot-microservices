package limitservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import limitservice.common.ConfigurationLimitService;
import limitservice.model.LimitConfigurationModel;

@RestController
public class LimitsConfigurationCtrl
{
	@Autowired
	private ConfigurationLimitService config;
	
	@GetMapping("/limits")
	public LimitConfigurationModel fetchLimitConfig()
	{
		return new LimitConfigurationModel(config.getMaximum(),config.getMinimum());
	}
}
